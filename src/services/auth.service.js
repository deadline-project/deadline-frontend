import http from "../http-common";

function login(username, password) {
    var data = {
        username: username,
        password: password
    };
    console.log(data)
    return http
        .post("/login", data)
        .then(response => {
            console.log(response.data)
        if (response.data.token) {
            localStorage.setItem('user', JSON.stringify(response.data)); // записываем данные пользователя в локальное хранилище, которое хранится в браузере
        }
        return response.data;
    });
}

function logout() {
    localStorage.removeItem('user'); // при нажатии кнопки "Выйти" удаляем данные пользователя из локального хранилища
}
function register(username, password, name) {
    var data = {
        username: username,
        password: password,
        name: name
    };
    return http.post("/register", data);
}

const exportedObject = {
    login: login,
    logout: logout,
    register: register
};

export default exportedObject;
