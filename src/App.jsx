import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';

import Header from './layout/Header'
import ListTasks from './pages/task/ListTasks'
import AddTask from './pages/task/AddTask'
import TaskData from './pages/task/TaskData'

import Login from "./pages/authorization/Login";
import Register from "./pages/authorization/Register";
import Profile from "./pages/authorization/Profile";
import { connect } from "react-redux";

function App({ user }) {
    return (
        <div>
            <BrowserRouter>
                <Header />
                <Routes>
                    <Route path='/listTasks' element={<ListTasks/>} />
                    <Route path='/addTask' element={<AddTask/>} />
                    <Route path="/task/:id" element={<TaskData/>}/>
                    <Route path="/login" element={<Login/>} />
                    <Route path="/register" element={<Register/>} />
                    <Route path="/profile" element={<Profile/>} />
                </Routes>
            </BrowserRouter>
        </div>
    );
}

function mapStateToProps(state) {
    const { user } = state.auth;
    return {
        user
    };
}

export default connect(mapStateToProps)(App);