import React, { useState } from 'react';
import http from "../../http-common";
import { Navigate } from 'react-router-dom';

function AddTask() {
    const [name, setName] = useState("");
    const [dateEnd, setDateEnd] = useState("");
    const [description, setDescription] = useState("");
    const [submitted, setSubmitted] = useState(false);

    const handleSubmit = (event) => {
        event.preventDefault();
        const data = {
            name: name,
            date_end: dateEnd,
            description: description
        };
        http
            .post("/addTask", data)
            .then(() => {
                setSubmitted(true);
            })
            .catch((e) => {
                console.log(e);
            });
    };

    return (
        <div className="container-md mt-3">
            <div className="col-sm-6">
                {!submitted ? (
                    <form onSubmit={handleSubmit}>
                        <div className="form-group">
                            <input
                                type="text"
                                name="name"
                                value={name}
                                placeholder="Наименование задачи"
                                onChange={(e) => setName(e.target.value)}
                                className="form-control"
                            />
                        </div>
                        <div className="form-group mt-2">
                            <input
                                type="date"
                                name="dateEnd"
                                value={dateEnd}
                                placeholder="Дедлайн"
                                onChange={(e) => setDateEnd(e.target.value)}
                                className="form-control"
                            />
                        </div>
                        <div className="form-group mt-2">
                            <textarea
                                name="description"
                                value={description}
                                placeholder="Описание задачи"
                                onChange={(e) => setDescription(e.target.value)}
                                className="form-control"
                            />
                        </div>
                        <button type="submit" className="btn btn-success mt-2">Добавить</button>
                    </form>
                ) : (
                    <Navigate to="/listTasks" />
                )}
            </div>
        </div>
    );
}

export default AddTask;