import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import http from "../../http-common";

function ListTasks() {
    const [tasks, setTasks] = useState([]);

    useEffect(() => {
        http
            .get("/tasks")
            .then(response => {
                setTasks(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }, []);

    return (
        <div className="container-md mt-3">
            <div className="col-sm-6">
                <Link to="/addTask" className="btn btn-success">Добавить задачу</Link>
                {tasks.length ? (
                    <ul className="list-group mt-3">
                        {tasks.map((task) => (
                            <Link to={`/task/${task.id}`} key={task.id} className="list-group-item list-group-item-action">
                                {task.name}
                            </Link>
                        ))}
                    </ul>
                ) : (
                    <div className="alert alert-info mt-3">Подождите, идёт загрузка данных</div>
                )}
            </div>
        </div>
    );
}

export default ListTasks;