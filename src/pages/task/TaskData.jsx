import React, { useState, useEffect }  from 'react';

import http from "../../http-common";

import { Navigate, useParams } from 'react-router-dom';

function TaskData() {

    const { id } = useParams();
    const [task, setTask] = useState({
        id: id,
        name: "",
        dateEnd: "",
        description: ""
    });

    const [submitted, setSubmitted] = useState(false);

    useEffect(() => {
        if (!id) {
            return;
        }

        function getTask() {
            http.get("/task/" + id)
                .then(response => {
                    setTask(prevTask => ({
                        ...prevTask,
                        id: response.data.id,
                        name: response.data.name,
                        dateEnd: response.data.date_end,
                        description: response.data.description
                    }));
                })
                .catch(e => {
                    console.log(e);
                });
        }

        getTask();
    }, [id]);


    function handleChange(event) {
        setTask({
            ...task,
            [event.target.name]: event.target.value
        });
    }

    function handleSubmit(event) {
        event.preventDefault();
        var data = {
            name: task.name,
            date_end: task.dateEnd,
            description: task.description
        };
        http
            .post("/updateTask/" + task.id, data)
            .then(() => {
                setSubmitted(true);
            })
            .catch(e => {
                console.log(e);
            });
    }

    function deleteBrand() {
        http
            .post("/deleteTask/" + task.id)
            .then(() => {
                setSubmitted(true);
            })
            .catch(e => {
                console.log(e);
            });
    }

    return (
        !submitted
            ?
        <div className="container-md mt-3">
            <div className="col-sm-6">
                <form onSubmit={handleSubmit}>
                    <div className="form-group">
                        <input
                            type="text"
                            name="name"
                            value={task.name}
                            placeholder="Наименование задачи"
                            onChange={handleChange}
                            className="form-control"
                        />
                    </div>
                    <div className="form-group mt-2">
                        <input
                            type="date"
                            name="dateEnd"
                            value={task.dateEnd}
                            placeholder="Дедлайн"
                            onChange={handleChange}
                            className="form-control"
                        />
                    </div>
                    <div className="form-group mt-2">
                            <textarea
                                name="description"
                                value={task.description}
                                placeholder="Описание задачи"
                                onChange={handleChange}
                                className="form-control"
                            />
                    </div>
                    <div className="btn-group mt-2">
                        <button type="submit" className="btn btn-success rounded">Обновить</button>
                        <button className="btn btn-danger  mx-1 rounded" onClick={deleteBrand}>Удалить</button>
                    </div>
                </form>

            </div>
        </div>
        : <Navigate to="/listTasks" />
    )
}

export default TaskData;